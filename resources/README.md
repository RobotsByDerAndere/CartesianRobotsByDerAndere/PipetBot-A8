# PipetBot-A8 schematics

Electrical connections are specified in the excerpt of the schematic circuit diagram of the PipetBot-A8 electronics: See ../resources/schematics/ (open file PipetBot-A8_schematics.pdf with a software that supports viewing portable document files (PDF); open file schematics/PipetBot-A8_KiCAD/PipetBot-A8_board.pro with KiCAD 5.1.10 and view file PipetBot-A8_board.sch in KiCAD 5.1.10). Note the differences in net labels when compared to the complete reverse engineered schematic circuit diagram of the original (unmodified) Anet V1.0 "Anet 1.5") board which can be found at [online](https://github.com/ralf-e/ANET-3D-Board-V1.0). 

# PumpAA

PumpAA (open file PumpAA1.pdf with a software that supports viewing portable 
document files (PDF); open file PumpAA.FCStd with [FreeCAD 0.18](https://www.freecadweb.org/wiki/Download/en)) 
can be used as the pipetting module of the PipetBot-A8 by DerAndere.
The parts in dark gray move relative to the light gray/transparent/brass parts
The Luer Lock ending of syringe cyliner "TubAB" should be connected to a pipette 
tip connector (made from a pipette tip or 3D-printed and coated 
with silicone tubing) via flexible tubing with less than 1mm inner 
diameter. The tubing can be filled with water except for the last 18 cm before 
the pipette tip connector.
The stepper motor "MtrAA" is connected to pins for the extruder motor 
on the Anet V1.0 controller board via cables.

# Licensing information

```
SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0
```

Copyright 2018 - 2021 DerAndere

This work as a whole is dual-licensed under the terms of the Apache License, Version 2.0
OR under the terms of the Creative Commons Attribution 4.0 International License. 
Third-party components are licensed under the original license provided by the owner 
of the applicable component. For detailled licensing information, see the .dep5 
file and ./LICENSES/ folder in the root directory of this work.

You may choose one of the following:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at 

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

OR: 

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ .
