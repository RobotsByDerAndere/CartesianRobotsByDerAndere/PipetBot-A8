# PipetBot-A8

PipetBot-A8 is a liquid handling robot that is based on an Anet A8 3D printer 
by Shenzhen Anet Technology Co.,Ltd with the original Firmware replaced by the 
customized branch "Marlin2forPipetBot" of my Marlin 2.0 firmware fork. 
The GitLab subgroup 
https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ 
contains the Project PipetBot-A8 with a DIY pipetting module (see resources/README.md) 
and prewritten G-code scripts that can serve as a template for user-written 
scripts and for testing the PipetBot-A8 hardware. This project depends on the following 
projects:
A)  GGCGen (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen), 
    My graphical G-code generator. It provides a graphical user interface 
    for robot control and semi-automatic G-code script generation. 
B)  Marlin2forPipetBot 
    (https://github.com/DerAndere1/Marlin/tree/Marlin2ForPipetBot), a branch of 
    my fork of the firmware Marlin 2.0 that makes it possible to extend the 
    possibilities of the PipetBot-A8 with additional hardware.

# Credits

DerAndere ([Donate](https://www.paypal.com/donate/?hosted_button_id=TNGG65GVA9UHE))

# Licensing information

```
SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0
```

Copyright 2018 - 2021 DerAndere

This work as a whole is dual-licensed under the terms of the Apache License, Version 2.0 
OR under the terms of the Creative Commons Attribution 4.0 International License. 
Third-party components are licensed under the original license provided by the owner 
of the applicable component. For detailled licensing information, see the .dep5 
file and ./LICENSES/ folder in the root directory of this work.

You may choose one of the following:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at 

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

OR: 

This work is licensed under the Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ .
