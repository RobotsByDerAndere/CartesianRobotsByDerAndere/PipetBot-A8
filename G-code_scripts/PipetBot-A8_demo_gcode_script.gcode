% G-Code script for an Anet A8-based liquid handling robot
; SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
; 
;     http://www.apache.org/licenses/LICENSE-2.0
; 
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
; 
; @title: PipetBot-A8_demo_gcode_script.gcode
; @author: DerAndere
; @date: 2018
; Copyright 2018 DerAndere
; @license: Apache-2.0 OR CC-BY-4.0
; @version: 1.2
; @about: This is a program written in G-code to test programming of the 
; PipetBot-A8, a self-made liquid handling robot / laser engraving machine 
; combination that is based on the an Anet A8 3D printer by Shenzhen Anet 
; Technology Co.,Ltd with the original Firmware replaced by the open source 
; firmware Marlin 2.0.x or newer (http://marlinfw.org/).
; @info: https://derandere.gitlab.io


O1000 ; Program sequence number 
; This preamble is mandatory. Keep the preset text at the beginning of the G-code script unchanged 
M302 S0 ; Allow cold extrusion / dispensing at all temperatures 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
; End of the mandatory preamble. Keep above preset text unchanged. User-defined G-code script is amended below. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; tip change 
 G1 Z230 F500 ; Retract tool to prevent collision with obstacles 
 G1 X180 Y180 F5000 ; move to waste position 
 G1 Z195 F500 ; Lower the top edge of the tip 15mm below the tip remover 
 G91 ; relative positioning 
 G1 X10 F1000 ; move in x towards tipRemover 
 G1 Y10 F1000 ; move towards tipRemover 
 G4 P250 ; Wait 250 milliseconds 
 G1 Y1 Z20 F500 ; Retract to remove pipet tip 
 G1 Y-1 Z-20 F500 ; repeat down 
 G1 Z20 F500 ; repeat up 
 G90 ; absolute positioning 
 G1 Y180 Z230 F500 ; Retract tool 
 G1 X19 Y197 F5000 ; Move the tool to tool holder position VH1(-3/125/120) 
 G1 Z102 F400 ; Move tool to tool position WT1M(-3/125/42) to attach pipet tip1 from spring-mounted tool holder 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z105 F500 ; Retract tool to tool position WT1H(-3/125/50) 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z100 F400 ; Move tool to tool position WT1L(-3/125/40) to firmly attach pipet tip1 from spring-mounted tool holder 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z230 F500 ; Retract tool 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel tube15mlRack5x1_2A1 
 G1 Z215 F500 ; move to z plane above target 
 G1 X27 Y23 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E-40 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A1 
 G1 X18 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E10 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A2 
 G1 Z83 F500 ; move to z plane above target 
 G1 X27 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E10 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A3 
 G1 Z83 F500 ; move to z plane above target
 G1 X36 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E10 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A4 
 G1 Z83 F500 ; move to z plane above target
 G1 X45 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E10 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; tip change 
 G1 Z230 F500 ; Retract tool to prevent collision with obstacles 
 G1 X180 Y180 F5000 ; move to waste position 
 G1 Z195 F500 ; Lower the top edge of the tip 15mm below the tip remover 
 G91 ; relative positioning 
 G1 X10 F1000 ; move in x towards tipRemover 
 G1 Y10 F1000 ; move towards tipRemover 
 G4 P250 ; Wait 250 milliseconds 
 G1 Y1 Z20 F500 ; Retract to remove pipet tip 
 G1 Y-1 Z-20 F500 ; repeat down 
 G1 Z20 F500 ; repeat up 
 G90 ; absolute positioning 
 G1 Y180 Z230 F500 ; Retract tool 
 G1 X28 Y197 F5000 ; Move the tool to tool holder position VH1(-3/125/120) 
 G1 Z102 F400 ; Move tool to tool position WT1M(-3/125/42) to attach pipet tip1 from spring-mounted tool holder 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z105 F500 ; Retract tool to tool position WT1H(-3/125/50) 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z100 F400 ; Move tool to tool position WT1L(-3/125/40) to firmly attach pipet tip1 from spring-mounted tool holder 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z230 F500 ; Retract tool 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel tube15mlRack5x1_2A2 
 G1 Z215 F500 ; move to z plane above target 
 G1 X46 Y23 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E-20 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A1 
 G1 X18 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; empty pump 
 G1 E0 F1000 ; Move piston to empty syringe pump. 
 G4 P500 ; Wait 500 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel vessel40x40x140_3A1 
 G1 Z215 F500 ; move to z plane above target 
 G1 X154 Y58 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight 14cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; wash1 clean3x 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight 14cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel tube15mlRack5x1_2A2 
 G1 Z215 F500 ; move to z plane above target 
 G1 X46 Y23 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E-20 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A2 
 G1 X27 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; empty pump 
 G1 E0 F1000 ; Move piston to empty syringe pump. 
 G4 P500 ; Wait 500 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel vessel40x40x140_3A1 
 G1 Z215 F500 ; move to z plane above target 
 G1 X154 Y58 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight 14cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; wash1 clean3x 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight 14cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel tube15mlRack5x1_2A2 
 G1 Z215 F500 ; move to z plane above target 
 G1 X46 Y23 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Dispense and wipe at vessel wall 
 G91 ; 
 G1 Z-3 F500 ; 
 G1 X5 F5000 ; 
 G1 E-20 F1000 ; 
 G1 Z3 F500 ; 
 G1 X-5 F5000 ; 
 G90 ; Set back to absolute positioning 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight tube_rack12cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A3 
 G1 X36 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; empty pump 
 G1 E0 F1000 ; Move piston to empty syringe pump. 
 G4 P500 ; Wait 500 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel vessel40x40x140_3A1 
 G1 Z215 F500 ; move to z plane above target 
 G1 X154 Y58 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight 14cm_[med-high] + (0) mm 
 G1 Z180 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; wash1 clean3x 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E220 F1000 ; Move piston to fill syringe pump with liquid 
 G4 P250 ; Wait 250 milliseconds 
 G1 E0 F1000 ; Move piston to dispense liquid 
 G4 P250 ; Wait 250 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight 14cm_[above] + (0) mm 
 G1 Z215 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; move to vessel plt96_1A4 
 G1 X45 Y118 F5000 ; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; Move to the predefined z-hight plate_96well_[high] + (0) mm 
 G1 Z77 F500 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; empty pump 
 G1 E0 F1000 ; Move piston to empty syringe pump. 
 G4 P500 ; Wait 500 milliseconds 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; tip change 
 G1 Z230 F500 ; Retract tool to prevent collision with obstacles 
 G1 X180 Y180 F5000 ; move to waste position 
 G1 Z195 F500 ; Lower the top edge of the tip 15mm below the tip remover 
 G91 ; relative positioning 
 G1 X10 F1000 ; move in x towards tipRemover 
 G1 Y10 F1000 ; move towards tipRemover 
 G4 P250 ; Wait 250 milliseconds 
 G1 Y1 Z20 F500 ; Retract to remove pipet tip 
 G1 Y-1 Z-20 F500 ; repeat down 
 G1 Z20 F500 ; repeat up 
 G90 ; absolute positioning 
 G1 Y180 Z230 F500 ; Retract tool 
 G1 X37 Y197 F5000 ; Move the tool to tool holder position VH1(-3/125/120) 
 G1 Z102 F400 ; Move tool to tool position WT1M(-3/125/42) to attach pipet tip1 from spring-mounted tool holder 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z105 F500 ; Retract tool to tool position WT1H(-3/125/50) 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z100 F400 ; Move tool to tool position WT1L(-3/125/40) to firmly attach pipet tip1 from spring-mounted tool holder 
 G4 P250 ; Wait 250 milliseconds 
 G1 Z230 F500 ; Retract tool 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; initialization 
 G1 Z230 F400 ; Retract tool to prevent collision with obstacles 
 G1 X0 Y0 Z230 F400 ; Move to secure-start position (0/0/210) 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 


