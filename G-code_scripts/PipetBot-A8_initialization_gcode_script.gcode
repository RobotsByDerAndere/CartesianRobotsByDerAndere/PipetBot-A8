% G-Code script for an Anet A8-based liquid handling robot
; SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
; 
;     http://www.apache.org/licenses/LICENSE-2.0
; 
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; @title: PipetBot-A8_initialization_gcode_script.gcode
; @author: DerAndere
; @date: 2018
; Copyright 2018 DerAndere
; @license: Apache-2.0 OR CC-BY-4.0
; @version: 1.0
; @about: This is a program written in G-code to test programming of the 
; PipetBot-A8, a self-made liquid handling robot / laser engraving machine 
; combination that is based on the an Anet A8 3D printer by Shenzhen Anet 
; Technology Co.,Ltd with the original Firmware replaced by the open source
; firmware Marlin 2.0.x or newer (http://marlinfw.org/).
; @info: https://derandere.gitlab.io

O1000 ; Program sequence number 
; This preamble is mandatory. Keep the preset text at the beginning of the G-code script unchanged 
M302 S0 ; Allow cold extrusion / dispensing at all temperatures 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
; End of the mandatory preamble. Keep above preset text unchanged. User-defined G-code script is amended below. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; initialization 
 G1 Z230 F300 ; Retract tool to prevent collision with obstacles 
 G1 X0 Y0 Z230 F300 ; Move to secure-start position (0/0/210) 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

