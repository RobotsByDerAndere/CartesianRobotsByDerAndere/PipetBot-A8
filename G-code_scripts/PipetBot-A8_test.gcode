% G-Code script for an Anet A8-based liquid handling robot
; SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
; 
;     http://www.apache.org/licenses/LICENSE-2.0
; 
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; @title: PipetBot-A8_test.gcode
; @author: DerAndere
; @date: 2018
; Copyright 2018 DerAndere
; @license: Apache-2.0 OR CC-BY-4.0
; @version: 1.2
; @about: This is a program written in G-code to test programming of the 
; PipetBot-A8, a self-made liquid handling robot / laser engraving machine 
; combination that is based on the an Anet A8 3D printer by Shenzhen Anet 
; Technology Co.,Ltd with the original Firmware replaced by the open source 
; firmware Marlin 2.0.x or newer (http://marlinfw.org/).
; @info: https://derandere.gitlab.io

O1000 ; Program sequence number

M302 S0 ; Allow cold extrusion at all temperatures

; Initialization-Test movement for Anet A8-based liquid handling robot
G1 Z120 F300 ; Retract tool to prevent collision with obstacles
G1 X0 Y0 Z120 F300 ; Move to secure-start position (0/0/120)
; G1 X0 Y0 Z0 F300 ; Move to origin (0/0/0)
; G1 X0 Y0 Z300 F400 ; secure-start
; G1 X30 Y0 Z300 F1000 ; Move in x direction to (30/0/200)
; G1 X30 Y30 Z300 F1000 ; Move in y direction to (30/30/200)

; Move to VW1 and waste disposal
G1 Z120 F300 ; Move to secure x-y-plane
G1 X170 Y200 Z120 F5000 ; Move in secure x-y plane to waste vessel position (VW1)(170/200/120)
G1 Z100 F300 ; Lower the tool to waste position VW1(170/200/100)
G1 E0 F400 ; Move piston to initial position E0 to dispose all liquid to waste 
G4 P500 ; Wait 500 milliseconds
    
; pipet tip renewal
    ; pipet tip disposal
G1 X170 Y215 Z100 F400 ; Move pipet tip tool in y direction under the tip ejector WE2(170/215/100)
G4 P250 ; Wait 250 milliseconds
G1 Z120 F300 ; Retract tool at ejector to eject pipet tip into waste 

    ; attach pipet tip1
G1 X-3 Y125 F5000 ; Move the tool to tool holder position VH1(-3/125/120)
G1 Z42 F300 ; Move tool to tool position WT1M(-3/125/42) to attach pipet tip1 from spring-mounted tool holder 
G4 P250 ; Wait 250 milliseconds
G1 Z50 F300 ; Retract tool to tool position WT1H(-3/125/50)
G4 P250 ; Wait 250 milliseconds
G1 Z40 F300 ; Move tool to tool position WT1L(-3/125/40) to firmly attach pipet tip1 from spring-mounted tool holder
G4 P250 ; Wait 250 milliseconds
G1 Z120 F300 ; Retract tool

; Take up liquid L2 from V1
G1 X170 Y50 Z 120 F5000 ; Move in secure x-y plane to V1_S(170/50/120)
G1 Z38 F300 ; Lower the tool to V1_L(170/50/38)
G1 E40 F400; take up x uL of L2 from V1
G4 P500 ; Wait 500 milliseconds     
G1 X165 Z55 F300 ; Move to V1_HE(165/50/55)
G1 Z60 F300 ; Retract to V1_HE(170/50/60)
G4 P250 ; Wait 250 milliseconds
G1 X170 Z120 F300 ; Retract tool

; Dispense Liquid L2 into V2a-c 
    ; Dispense Liquid into V2a and hold
G1 X-3 Y38 Z120 F5000 ; Move in secure x-y plane to V2a_s(-3/38/120)
G1 Z38 F400 ; lower tool to V2a_M(-3/38/38)
G91 ; Set to relative positioning for dispension of L2 into V2a
G1 E-10 F400 ; move piston to dispense y uL L2 to V2a 
G4 P500 ; Wait 500 milliseconds
G90 ; Set to absolute positioning for movement
G1 X-1 F1000 ; Move to wall of Vessel V2a_ME(-1/38/38)
G1 Z47 F300 ; Retract to V2a_HE(-1/38/47)
G4 P250 ; Wait 250 milliseconds
G1 X-3 Z55 F300 ; Retract from V2a an hold at V2a_P(-3/38/55)

    ; Further Dispense Liquid L2 into V2b and hold
G91 ; Set to relative positioning
G1 Y9 F1000 ; Direct Move to next vial V2b_P
G90 ; Set to absolute positioning
G1 Z38 F300; lower tool to V2b_M
G91 ; Set to relative positioning for dispension of L1 at V2b
G1 E-10 F400 ; move piston to dispense y uL L2 to V2b 
G4 P500 ; Wait 500 milliseconds
G1 X2 F1000 ; Move to wall of Vessel V2b_ME
G90
G1 Z47 F300 ; Retract to V2b_HE
G4 P250 ; Wait 250 milliseconds
G91
G1 X-2 F1000
G90
G1 Z55 F300 ; Retract tool from V2b and hold at V2b_P

    ; Further Dispense Liquid L2 into V2c and retract
G91 ; Set to relative positioning
G1 Y9 F1000 ; Direct Move to next vial V2c_P
G90 ; Set to absolute positioning
G1 Z38 F300; lower tool to V2c_M
G91 ; Set to relative positioning for dispension of L1 at V2c
G1 E-10 F400 ; move piston to dispense y uL L2 to V2c 
G4 P500 ; Wait 500 milliseconds
G1 X2 F1000 ; Move to wall of Vessel V2c_ME
G90
G1 Z47 F300 ; Retract to V2c_HE
G4 P250 ; Wait 250 milliseconds
G91
G1 X-2 F1000
G90
G1 Z120 F300 ; Retract tool

; dispense residual liquid L2 back into V1
G1 X170 Y50 F5000 ; Move in secure x-y plane to V1_S(170/50/120)
G1 Z50 F400 ; Lower the tool to V1_H(170/50/50)
G1 E0 F400 ; Dispense residual L2 back into V1
G4 P500 ; Wait 500 milliseconds
G1 X165 F1000 ; Move to wall V1_HE(165/50/50) 
G1 Z60 F300 ; Retract to V1_RE(165/50/60)
G4 P250 ; Wait 250 milliseconds  
G1 X170 Z140 F300 ; Retract tool

; Tool cleaning (3x)
G1 X170 Y160 Z140 F5000 ; Move in secure x-y plane to cleaning vessel position VCa_S(170/160/140)
G1 Z60 F400 ; Lower the tool to VCa_L(170/160/60) 
G1 E50 F400 ; Move piston to fill syringe pump with liquid L1 from VCa 
G4 P250 ; Wait 250 milliseconds
G1 E0 F400 ; Move piston to dispense liquid L1 into VCa
G4 P250 ; Wait 250 milliseconds
G1 E50 F400 ; Move piston to fill syringe pump with liquid L1 from VCa 
G4 P250 ; Wait 250 milliseconds
G1 E0 F400 ; Move piston to dispense liquid L1 into VCa
G4 P250 ; Wait 250 milliseconds
G1 E50 F400 ; Move piston to fill syringe pump with liquid L1 from VCa 
G1 X165 Z80 F300 ; Retract tool from liquid L1 to rim VCa_ME
G1 E0 F400 ; Move piston to dispense liquid L1 into VCa
G4 P500 ; Wait 500 milliseconds 
G1 Z85 F300 ; Move to VCa_HE(170/160/85)
G4 P250 ; Wait for 250 milliseconds
G1 X170 Z140 F300 ; Retract tool

; Mix
G1 X150 Y128 F5000 ;
G4 P20 ;
G1 Y123 F5000 ;
G4 P20 ;
G1 Y128 F5000 ;
G4 P20 ;
G1 Y123 F5000 ;
G4 P20 ;
G1 Y128 F5000 ;
G4 P20 ;
G1 Y123 F5000 ;
G4 P20 ;
G1 Y128 F5000 ;
G4 P20 ;
G1 Y123 F5000 ;
G4 P20 ;
G1 Y128 F5000 ;
G4 P20 ;
G1 Y123 F5000 ;

; Secure-End
G1 Z200 F300 ; retract tool to safe x-y-plane
G1 X0 Y0 F1000 ; move to secure start/ secure end position (0/0/200)


%

